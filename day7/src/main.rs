use std::fs::read_to_string;
use regex::Regex;
use petgraph::graphmap::DiGraphMap;
use petgraph::Direction;
use std::collections::HashSet;

fn part1(entries: &Vec<(String, String)>) {
    let mut graph = DiGraphMap::new();
    for (prereq, step) in entries.iter() {
        graph.add_node(prereq);
        graph.add_node(step);
        graph.add_edge(prereq, step, 0);
    }
    let mut steps = String::new();
    while graph.node_count() > 0 {
        let mut ready_steps = Vec::new();
        for node in graph.nodes() {
            if graph.neighbors_directed(node, Direction::Incoming).count() == 0 {
                ready_steps.push(node);
            }
        }
        ready_steps.sort();
        steps += ready_steps[0];
        graph.remove_node(ready_steps[0]);
    }

    println!("Step Order: {}", steps);
}

fn part2(entries: &Vec<(String, String)>) {
    let mut graph = DiGraphMap::new();

    for (prereq, step) in entries.iter() {
        graph.add_node(prereq);
        graph.add_node(step);
        graph.add_edge(prereq, step, 0);
    }
    let mut time: u64 = 0;
    let mut workers = Vec::new();
    let mut working = HashSet::new();
    while graph.node_count() > 0 {
        let mut ready_steps = Vec::new();
        for node in graph.nodes() {
            if !working.contains(node) && graph.neighbors_directed(node, Direction::Incoming).count() == 0 {
                ready_steps.push(node);
            }
        }
        ready_steps.sort();
        for node in ready_steps {
            workers.push((node, node.as_bytes()[0] - 4));
            working.insert(node);
            if workers.len() == 5 {
                break;
            }
        }

        workers.sort_by(|a,b| a.1.cmp(&b.1));
        let shortest = workers[0].1;
        time += u64::from(shortest);
        graph.remove_node(workers[0].0);
        working.remove(workers[0].0);
        println!("Time: {} {:?}", time, workers);
        for i in 1..workers.len() {
            workers[i].1 -= shortest;
        }

        workers = (&workers[1..]).to_vec();
    }
    while workers.len() > 0 {
        let shortest = workers[0].1;
        println!("{:?}", workers);
        time += u64::from(shortest);
        for i in 1..workers.len() {
            workers[i].1 -= shortest;
        }

        workers = (&workers[1..]).to_vec();
    }
    println!("Time {}", time);
}

fn main() {

    let data = read_to_string("input").expect("Failed to open file");
    let re = Regex::new(r"Step (.*) must be finished before step (.*) can begin.").expect("Regex");
    let entries = data.lines().map(|line| {
        let captures = re.captures(line).unwrap();
        ((&captures[1]).to_string(), (&captures[2]).to_string())
    }).collect::<Vec<_>>();
    part1(&entries);
    part2(&entries);
}