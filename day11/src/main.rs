fn calculate_power(x: i32, y: i32, serial: i32) -> i32 {
    let rackid = x + 10;
    let mut powerlevel = rackid * y;
    powerlevel += serial;
    powerlevel *= rackid;
    let digit = (powerlevel / 100i32) % 10i32;
    digit - 5
}

fn generate_grid() -> Vec<Vec<i32>> {
    let mut grid = vec![vec![0i32; 300]; 300];
    for y in 0..300 {
        for x in 0..300 {
            grid[x][y] = calculate_power((x + 1) as i32, (y + 1) as i32,5153);
        }
    }
    grid
}


fn sum_power(x_coord: usize, y_coord: usize, size: usize, grid: &Vec<Vec<i32>>) -> i32 {
    let mut sum = 0;
    if x_coord + size < grid.len() && y_coord + size < grid.len() {
        for y in y_coord..y_coord+size {
            for x in x_coord..x_coord + size {
                sum += grid[x][y];
            }
        }
    }
    sum
}


fn part1() {
    let grid = generate_grid();
    let mut sum = 0;
    let mut x_coord = 0;
    let mut y_coord = 0;

    for y in 0..300 {
        for x in 0..300 {
            let val = sum_power(x, y, 3, &grid);
            if val > sum {
                sum = val;
                x_coord = x+1;
                y_coord = y+1;
            }
        }
    }
    println!("{},{} {}", x_coord, y_coord, sum);
}

fn part2() {
    let grid = generate_grid();
    let mut sum = 0;
    let mut x_coord = 0;
    let mut y_coord = 0;
    let mut square_size = 0;
    for y in 0..300 {
        for x in 0..300 {
            for size in 1..300 {
                let val = sum_power(x, y, size, &grid);
                if val > sum {
                    sum = val;
                    x_coord = x + 1;
                    y_coord = y + 1;
                    square_size = size
                }
            }
        }
    }
    println!("{},{},{} {}", x_coord, y_coord, square_size, sum);
}

fn main() {
    let grid = generate_grid();
    part1();
    part2();
}
