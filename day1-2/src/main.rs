extern crate failure;

use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader, Read};
use failure::Error;

fn main() -> Result<(), Error> {
    let mut data = Vec::new();
    let mut file = File::open("input")?;
    file.read_to_end(&mut data)?;
    let mut sums = HashSet::new();
    let mut sum: i64 = 0;
    sums.insert(sum);

    loop {
        let reader = BufReader::new(data.as_slice());
        for line in reader.lines() {
            let line = line?;
            let val: i64 = line.parse()?;
            sum += val;
            {
                if let Some(found) = sums.get(&sum) {
                    println!("Matched: {}", found);
                    return Ok(())
                }
            }
            sums.insert(sum);
        }
    }

}