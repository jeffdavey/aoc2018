use std::fs::read_to_string;
use regex::Regex;
use std::collections::VecDeque;

fn get_pos(len: i32, current: i32, adjust: i32) -> usize {
    let val = current + adjust;
    let result = {
        if val > len {
            (val - len) as usize
        } else if val < 0 {
            (len + val) as usize
        } else {
            val as usize
        }
    };
    result
}

fn part1(max_players: u32, last_marble: u32) -> u32 {
    let mut marbles = VecDeque::new();
    marbles.push_back(0 as u32);
    let mut players = vec![0; max_players as usize];
    let mut current_player = 0;
    let mut current_marble = 0;
    for i in 1..last_marble {
        if i % 23 == 0 {
            players[current_player] += i;
            let pos = get_pos(marbles.len() as i32, current_marble as i32, -7);
            players[current_player] += marbles[pos];
            marbles.remove(pos);
            if pos >= marbles.len() {
                current_marble = 0;
            } else {
                current_marble = pos;
            }
        } else {
            let pos = get_pos(marbles.len() as i32, current_marble as i32, 2);
            marbles.insert(pos, i);
            current_marble = pos;
        }
        current_player += 1;
        if current_player >= max_players as usize {
            current_player = 0;
        }
    }

    *players.iter().max().unwrap()
}

fn main() {
    let data = read_to_string("input").expect("Failed to open file");
    let re = Regex::new(r"(\d+) players; last marble is worth (\d+) points").unwrap();
    let captures = re.captures(&data).unwrap();
    let players = captures[1].parse::<u32>().unwrap();
    let last_marble = captures[2].parse::<u32>().unwrap();

    println!("High score {}", part1(players, last_marble * 100));

}