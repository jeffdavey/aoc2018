extern crate failure;

use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader, Read};
use failure::Error;

fn main() -> Result<(), Error> {
    let mut data = Vec::new();
    let mut file = File::open("input")?;
    file.read_to_end(&mut data)?;

    let mut istwo = 0;
    let mut isthree = 0;
    let reader = BufReader::new(data.as_slice());
    for line in reader.lines() {
        let line = line?;
        let mut map = HashMap::new();
        for c in line.chars() {
            if !map.contains_key(&c) {
                map.insert(c, 1);
            } else {
                let val = map.get_mut(&c).unwrap();
                *val += 1;
            }
        }
        let mut has_two = false;
        let mut has_three = false;
        // go through the map and set vals
        for (c,val) in &map {
            if *val == 3 {
                has_three = true;
            } else if *val == 2 {
                has_two = true;
            }
        }
        if has_three {
            isthree += 1;
        }
        if has_two {
            istwo += 1;
        }
    }
    println!("Checksum: {}", isthree * istwo);
    Ok(())
}