extern crate regex;
extern crate lazy_static;
extern crate chrono;

use std::fs::read_to_string;
use regex::Regex;
use lazy_static::lazy_static;
use chrono::prelude::*;
use std::cmp::Ordering;
use std::collections::HashMap;

#[derive(Eq, Debug)]
struct TimeEntry {
    time: NaiveDateTime,
    entry: Entry
}

impl TimeEntry {
    pub fn parse(line: &str) -> Option<Self> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"\[(\d+\-\d+\-\d+ \d+:\d+)\]\s(.+)").unwrap();
        }
        if let Some(captures) = RE.captures(line) {
            if let Some(entry) = Entry::parse(&captures[2]) {
                return Some(TimeEntry {
                    time: NaiveDateTime::parse_from_str(&captures[1], "%Y-%m-%d %H:%M").unwrap(),
                    entry
                })
            }
        }
        None
    }
}

impl Ord for TimeEntry {
    fn cmp(&self, other: &TimeEntry) -> Ordering {
        self.time.cmp(&other.time)
    }
}

impl PartialOrd for TimeEntry {
    fn partial_cmp(&self, other: &TimeEntry) -> Option<Ordering> {
        self.time.partial_cmp(&other.time)
    }
}

impl PartialEq for TimeEntry {
    fn eq(&self, other: &TimeEntry) -> bool {
        self.time == other.time
    }
}

#[derive(Eq, PartialEq, Debug)]
enum Entry {
    StartShift(u32),
    WakeUp,
    FallAsleep,
}

impl Entry {
    pub fn parse(line: &str) -> Option<Self> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r".*Guard #(\d+) begins shift").unwrap();
        }
        if line == "wakes up" {
            Some(Entry::WakeUp)
        } else if line == "falls asleep" {
            Some(Entry::FallAsleep)
        } else {
            if let Some(captured) = RE.captures(line) {
                Some(Entry::StartShift(captured[1].parse().unwrap()))
            } else {
                None
            }
        }
    }
}

fn main() {
    let data = read_to_string("input").expect("Failed to open file");
    let mut entries: Vec<TimeEntry> = data.lines().filter_map(|line| TimeEntry::parse(line)).collect();
    entries.sort();
    let mut schedule = HashMap::new();
    let mut sleepmin = 0;
    let mut current_guard= 0;
    for entry in entries {
        match entry.entry {
            Entry::StartShift(guardid) => {
                current_guard = guardid;
                if !schedule.contains_key(&guardid) {
                    schedule.insert(guardid, [0u16; 60]);
                }
            },
            Entry::FallAsleep => {
                sleepmin = entry.time.minute();
            },
            Entry::WakeUp => {
                let current_min = entry.time.minute();
                let current_schedule = schedule.get_mut(&current_guard).unwrap();
                for i in sleepmin..current_min {
                    current_schedule[i as usize] += 1;
                }
            }
        }
    }

    let mut most_id = 0;
    let mut most_asleep = 0;
    let mut most_min: u32 = 0;
    let mut all_most_min: u32 = 0;
    let mut all_most_minval = 0;
    let mut all_most_guard = 0;
    for (id, minutes) in &schedule {
        let mut total = 0;
        let mut highest = 0;
        let mut highest_min: u32 = 0;
        for i in 0..minutes.len() {
            if *id == 863 {
                println!("min {} {}", i, minutes[i]);
            }
            let minute = minutes[i];
            total += minute;
            if minute > highest {
                highest = minute;
                highest_min = i as u32;
            }
        }
        println!("Guard {} {}", *id, total);
        if total > most_asleep {
            most_id = *id;
            most_asleep = total;
            most_min = highest_min;
        }
        if highest > all_most_minval {
            all_most_minval = highest;
            all_most_min = highest_min;
            all_most_guard = *id;
        }
    }

    println!("Guard {} slept {} min {} val {}", most_id, most_asleep, most_min,most_id * most_min);
    println!("Guard {} slept {} min at {} val {}", all_most_guard, all_most_min, all_most_minval, all_most_guard * all_most_min);
}
