use std::fs::read_to_string;
use regex::Regex;

struct Vec2 {
    x: i32,
    y: i32,
}


fn get_diffs(points: &Vec<(Vec2, Vec2)>) -> (i32, i32) {
    let mut low_x = 0;
    let mut low_y  = 0;
    let mut high_x = 0;
    let mut high_y = 0;
    for (point, _) in points {
        if point.x < low_x {
            low_x = point.x;
        } else if point.x > high_x {
            high_x = point.x;
        }
        if point.y < low_y {
            low_y = point.y;
        } else if point.y > high_y {
            high_y = point.y;
        }
    }

    let x_diff = high_x - low_x;
    let y_diff = high_y - low_y;

    (x_diff, y_diff)
}

fn main() {
    let data = read_to_string("input").expect("Failed to open file");
    let re = Regex::new(r"position=<([\s-]\d+), ([\s-]\d+)> velocity=<([\s-]\d+), ([\s-]\d+)>").unwrap();
    let mut points = data.lines().map(|l| {
        let captures = re.captures(l).unwrap();
        let pos = Vec2 {
            x: captures[1].trim().parse::<i32>().unwrap(),
            y: captures[2].trim().parse::<i32>().unwrap(),
        };
        let vel = Vec2 {
            x: captures[3].trim().parse::<i32>().unwrap(),
            y: captures[4].trim().parse::<i32>().unwrap(),
        };
        (pos, vel)
    }).collect::<Vec<_>>();

    let mut x_diff = 0;
    let mut y_diff = 0;

    let mut seconds = 0;
    loop {
        for (pos, vel) in &mut points {
            pos.x += vel.x;
            pos.y += vel.y;
        }
        let (new_x, new_y) = get_diffs(&points);
        if x_diff == 0 || (new_x < x_diff && new_y < y_diff) {
            x_diff = new_x;
            y_diff = new_y;
        } else {
            break;
        }
        seconds += 1;
    }
    for (pos, vel) in &mut points {
        pos.x -= vel.x;
        pos.y -= vel.y;
    }

    println!("secs {}", seconds);
    let mut grid = vec![vec!['.'; y_diff as usize]; x_diff as usize];
    for (pos, vel) in &mut points {
        grid[(x_diff - pos.x) as usize][(y_diff - pos.y) as usize] = '#';
    }

    for y in 0..grid[0].len() {
        for x in 0..grid.len() {
            print!("{}", grid[x][y]);
        }
        println!()
    }
}