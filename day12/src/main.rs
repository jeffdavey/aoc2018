use std::fs::read_to_string;

struct Pattern {
    pattern: Vec<char>,
    result: char
}

fn map_state(state: String) -> Vec<char> {
    let mut map = Vec::new();

    map.push('.');
    map.push('.');
    map.push('.');
    for c in state.chars() {
        map.push(c);
    }
    map.push('.');
    map.push('.');
    map.push('.');
    map
}

fn matches(a: &[char], b: &[char]) -> bool {
    for i in 0..a.len() {
        if a[i] != b[i] {
            return false;
        }
    }
    return true;
}

fn apply_generation(state: &Vec<char>, patterns: &Vec<Pattern>, center: &mut usize) -> Vec<char> {
    let mut new_state = Vec::new();
    new_state.push('.');
    new_state.push('.');
    for s in 2..state.len() - 2 {
        let mut matched = false;
        for pattern in patterns {

            let left = s - 2;
            let right = s + 2;

            if matches(&state[left..=right], &pattern.pattern) {
                matched = true;
                new_state.push(pattern.result);
                break;
            }
        }
        if !matched {
            new_state.push(state[s]);
        }
    }
    if new_state[2] == '#' {
        new_state.insert(0, '.');
        *center += 1;
    }
    new_state.push( '.');
    new_state.push('.');
    new_state.push('.');

    new_state

}

fn sum(state: &Vec<char>, center: usize) -> i64 {
    let mut sum: i64 = 0;
    for i in 0..state.len() {
        if state[i] == '#' {
            sum += i as i64 - center as i64;
        }
    }
    sum
}

fn part1(state: &Vec<char>, patterns: &Vec<Pattern>) {
    let mut state = state.to_vec();
    let mut center = 3;
    for _ in 0..20 {
        state = apply_generation(&state, patterns, &mut center);
    }
    println!("part1: {}", sum(&state, center));
}

fn part2(state: &Vec<char>, patterns: &Vec<Pattern>) {
    let mut state = state.to_vec();
    let mut center = 3;
    for i in 0..300 {
        state = apply_generation(&state, patterns, &mut center);
        print(&state, i, center);
    }
    // should be converged, get sum:
    let suma = sum(&state, center);
    state = apply_generation(&state, patterns, &mut center);
    let sumb = sum(&state, center);

    println!("part2: {} {}", sumb - suma, suma + ((sumb - suma) * 49_999_999_700));
}


fn print(state: &Vec<char>, generation: usize, center: usize) {
    print!("{:2}", generation);
    for s in state {
        print!("{}", s);
    }
    println!();
    print!("{:2}", generation);
    for _ in 0..center {
        print!("-")
    }
    println!("^");

}

fn main() {
    let data = read_to_string("input").expect("Failed to open input");
    let lines = data.lines();
    let mut initial= String::new();
    let patterns = lines.filter_map(|l| {
        if l.starts_with("initial state:") {
            initial = (&l["initial state: ".len()..]).to_string();
            None
        } else if l.is_empty() {
            None
        } else {
            let mut pattern = l.split("=>");
            Some(Pattern {
                pattern: pattern.next().unwrap().trim().to_string().chars().collect::<Vec<_>>(),
                result: (pattern.next().unwrap().trim()).chars().next().unwrap(),
            })
        }
    }).collect::<Vec<_>>();
    let state = map_state(initial);
    print(&state, 0, 3);
    part1(&state, &patterns);
    part2(&state, &patterns);





}
