use std::fs::read_to_string;
use std::collections::HashSet;

fn main() {
    let data = read_to_string("input").expect("Failed to open file");
    let data = data.lines().map(|line| line.split(",").map(|s| s.trim().parse::<usize>().unwrap()).collect::<Vec<usize>>()).collect::<Vec<Vec<usize>>>();
    let max_x = data.iter().max_by_key(|coord| coord[0]).unwrap()[0];
    let max_y = data.iter().max_by_key(|coord| coord[1]).unwrap()[1];

    let mut coordinates = vec![vec![".".to_string(); max_y + 1]; max_x + 1];
    data.iter().enumerate().for_each(|(index, coord)| println!("{} {},{}", index, coord[0], coord[1]));
    data.iter().enumerate().for_each(|(index, coord)| coordinates[coord[0]][coord[1]] = index.to_string());

    for x in 0..max_x {
        for y in 0..max_y {
            if coordinates[x][y] == "." {
                let mut tie = false;
                let mut closest_val = ".".to_string();
                let mut closest = i32::max_value();
                for (index, coord) in data.iter().enumerate() {
                    let distance = (coord[0] as i32 - x as i32).abs() + (coord[1] as i32 - y as i32).abs();
                    if distance < closest {
                        closest = distance;
                        closest_val = index.to_string();
                        tie = false;
                    } else if distance == closest {
                        tie = true;
                    }
                }
                if !tie {
                    coordinates[x][y] = closest_val;
                }
            }
        }
    }

    // remove edges
    let mut blacklist = HashSet::new();
    for x in 0..max_x {
        blacklist.insert(coordinates[x][max_y].to_string());
        blacklist.insert(coordinates[x][0].to_string());
    }
    for y in 0..max_y {
        blacklist.insert(coordinates[max_x][y].to_string());
        blacklist.insert(coordinates[0][y].to_string());
    }

    // now find biggest
    let mut areas = vec![0; data.len()];
    for x in 0..max_x {
        for y in 0..max_y {
            if coordinates[x][y] != "." && !blacklist.contains(&coordinates[x][y]) {
                let index = coordinates[x][y].parse::<usize>().unwrap();
                areas[index] += 1;
            }
        }
    }
    println!("part1 max {}", areas.iter().max().unwrap());

    let mut area = 0;
    for x in 0..max_x {
        for y in 0..max_y {
            let mut total_distance = 0;
            for coord in data.iter() {
                total_distance += (coord[0] as i32 - x as i32).abs() + (coord[1] as i32 - y as i32).abs();
            }
            if total_distance < 10_000 {
                area += 1;
            }
        }
    }
    println!("part2 area: {}", area);
}