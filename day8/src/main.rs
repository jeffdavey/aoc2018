use std::fs::read_to_string;

fn part1(entries: &Vec<u32>, index: &mut u32) -> u32 {
    let mut work_index = *index;
    let mut sum = 0;
    let children = entries[work_index as usize];
    work_index += 1;
    let meta = entries[work_index as usize];
    work_index += 1;
    for _ in 0..children {
        sum += part1(entries, &mut work_index);
   }

    for _ in 0..meta {
        sum += entries[work_index as usize];
        work_index += 1;
    }
    *index = work_index;
    sum
}

fn part2(entries: &Vec<u32>, index: &mut u32) -> u32 {
    let mut work_index = *index;
    let mut sum = 0;
    let children = entries[work_index as usize];
    work_index += 1;
    let meta = entries[work_index as usize];
    work_index += 1;
    let mut children_sums = vec![0u32; children as usize];
    for i in 0..children {
        children_sums[i as usize] = part2(entries, &mut work_index);
    }

    for _ in 0..meta {
        let entry = entries[work_index as usize];
        if children > 0 {
            if entry > 0 && entry <= children {
                sum += children_sums[(entry - 1) as usize]
            }
        } else {
            sum += entries[work_index as usize];
        }
        work_index += 1;
    }
    *index = work_index;
    sum
}

fn main() {
    let data = read_to_string("input").expect("Failed to open file");
    let entries = data.split_whitespace().map(|s| s.parse::<u32>().unwrap()).collect::<Vec<_>>();
    let mut index = 0;
    println!("Part1 Sum {}", part1(&entries, &mut index));
    index = 0;
    println!("Part2 Sum {}", part2(&entries, &mut index));
}
