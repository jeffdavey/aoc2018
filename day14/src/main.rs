

fn do_recipe(elf1: &mut usize, elf2: &mut usize, recipes: &mut Vec<String>) {
    let recipe1 = recipes[*elf1].parse::<usize>().expect("Parsed input failed");
    let recipe2 = recipes[*elf2].parse::<usize>().expect("Parsed input failed");
    let sum = recipe1 + recipe2;
    let scorea = sum % 10;
    if sum >= 10 {
        let scoreb = sum / 10 % 10;
        recipes.push(scoreb.to_string());
    }
    recipes.push(scorea.to_string());
    *elf1 += (1 + recipe1);
    *elf2 += (1 + recipe2);
    if *elf1 >= recipes.len() {
        *elf1 %= recipes.len();
    }
    if *elf2 >= recipes.len() {
        *elf2 %= recipes.len();
    }
}

fn part1(input: String, num_first: usize, num_recipes: usize) {
    let mut recipes= input.chars().map(|c| c.to_string()).collect::<Vec<_>>();
    let mut elf1 = 0;
    let mut elf2 = 1;

    while recipes.len() < num_first {
        do_recipe(&mut elf1, &mut elf2, &mut recipes);
    }

    let len = num_first;
    while recipes.len() - len < num_recipes {
        do_recipe(&mut elf1, &mut elf2, &mut recipes);
    }

    let input = &recipes[len..(len + num_recipes)].concat();

    println!("{:?}", input);
}

fn part2(input: String, sequence: String) {
    let sequence = sequence.chars().map(|c| c.to_string()).collect::<Vec<_>>();
    let mut recipes= input.chars().map(|c| c.to_string()).collect::<Vec<_>>();
    let mut elf1 = 0;
    let mut elf2 = 1;
    let mut search_index = recipes.len();
    loop {
        do_recipe(&mut elf1, &mut elf2, &mut recipes);
        if (recipes.len() - search_index) > sequence.len() {
            if &recipes[search_index..(search_index+sequence.len())] == &sequence[0..] {
                println!("Found at {}", search_index);
                break;
            }
            else {
                search_index += 1;
            }
        }
    }
}

fn main() {
    part1("37".to_string(), 793031, 10);
    part2("37".to_string(), "793031".to_string());
}
