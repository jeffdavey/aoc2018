use std::fs::read_to_string;
use std::collections::HashMap;
use regex::{Regex, Captures};

#[derive(Debug, Default)]
struct Sample {
    before: [usize; 4],
    op: [usize; 4],
    after: [usize; 4]
}

impl Sample {
    pub fn new() -> Self {
        Self {
            ..Default::default()
        }
    }

    pub fn set_before(&mut self, before: [usize; 4]) {
        self.before = before;
    }

    pub fn set_op(&mut self, op: [usize; 4]) {
        self.op = op;
    }

    pub fn set_after(&mut self, after: [usize; 4]) {
        self.after = after;
    }

    pub fn before(&self) -> [usize; 4] {
        self.before
    }

    pub fn op(&self) -> [usize; 4] {
        self.op
    }

    pub fn after(&self) -> [usize; 4] {
        self.after
    }
}


#[derive(Debug, Default, Copy, Clone)]
struct Computer {
    pub registers: [usize; 4],
}

impl Computer {
    pub fn new() -> Self {
        Self {
            ..Default::default()
        }
    }

    pub fn from_arr(arr: [usize; 4]) -> Self {
        Self {
            registers: arr
        }
    }

    pub fn addr(&mut self, a: usize, b: usize, c: usize) {
        self.registers[c] = self.registers[a] + self.registers[b];
    }

    pub fn addi(&mut self, a: usize, b: usize, c: usize) {
        self.registers[c] = self.registers[a] + b;
    }

    pub fn mulr(&mut self, a: usize, b: usize, c: usize) {
        self.registers[c] = self.registers[a] * self.registers[b];
    }

    pub fn muli(&mut self, a: usize, b: usize, c: usize) {
        self.registers[c] = self.registers[a] * b;
    }

    pub fn banr(&mut self, a: usize, b: usize, c: usize) {
        self.registers[c] = self.registers[a] & self.registers[b];
    }

    pub fn bani(&mut self, a: usize, b: usize, c: usize) {
        self.registers[c] = self.registers[a] & b;
    }

    pub fn borr(&mut self, a: usize, b: usize, c: usize) {
        self.registers[c] = self.registers[a] | self.registers[b];
    }

    pub fn bori(&mut self, a: usize, b: usize, c: usize) {
        self.registers[c] = self.registers[a] | b;
    }

    pub fn setr(&mut self, a: usize, _b: usize, c: usize) {
        self.registers[c] = self.registers[a];
    }

    pub fn seti(&mut self, a: usize, _b: usize, c: usize) {
        self.registers[c] = a;
    }

    pub fn gtir(&mut self, a: usize, b: usize, c: usize) {
        self.registers[c] = if a > self.registers[b] { 1 } else { 0 };
    }

    pub fn gtri(&mut self, a: usize, b: usize, c: usize) {
        self.registers[c] = if self.registers[a] > b { 1 } else { 0 };
    }

    pub fn gtrr(&mut self, a: usize, b: usize, c: usize) {
        self.registers[c] = if self.registers[a] > self.registers[b] { 1 } else { 0 };
    }

    pub fn eqir(&mut self, a: usize, b: usize, c: usize) {
        self.registers[c] = if a == self.registers[b] { 1 } else { 0 };
    }

    pub fn eqri(&mut self, a: usize, b: usize, c: usize) {
        self.registers[c] = if self.registers[a] == b { 1 } else { 0 };
    }

    pub fn eqrr(&mut self, a: usize, b: usize, c: usize) {
        self.registers[c] = if self.registers[a] == self.registers[b] { 1 } else { 0 };
    }

    pub fn exec(&mut self, mapping: &HashMap<usize, String>, op: &[usize; 4]) {
        let name = mapping.get(&op[0]).unwrap();
        match name.as_str() {
            "addr" => self.addr(op[1], op[2], op[3]),
            "addi" => self.addi(op[1], op[2], op[3]),
            "mulr" => self.mulr(op[1], op[2], op[3]),
            "muli" => self.muli(op[1], op[2], op[3]),
            "banr" => self.banr(op[1], op[2], op[3]),
            "bani" => self.bani(op[1], op[2], op[3]),
            "borr" => self.borr(op[1], op[2], op[3]),
            "bori" => self.bori(op[1], op[2], op[3]),
            "setr" => self.setr(op[1], op[2], op[3]),
            "seti" => self.seti(op[1], op[2], op[3]),
            "gtir" => self.gtir(op[1], op[2], op[3]),
            "gtri" => self.gtri(op[1], op[2], op[3]),
            "gtrr" => self.gtrr(op[1], op[2], op[3]),
            "eqir" => self.eqir(op[1], op[2], op[3]),
            "eqri" => self.eqri(op[1], op[2], op[3]),
            "eqrr" => self.eqrr(op[1], op[2], op[3]),
            _ => unreachable!()
        }

    }
}

enum SampleParseState {
    Before,
    After,
    Opcode
}

fn captures_to_arr<'t>(captures: &Captures<'t>) -> [usize; 4] {
    let a: usize = captures[1].parse().unwrap();
    let b: usize = captures[2].parse().unwrap();
    let c: usize = captures[3].parse().unwrap();
    let d: usize = captures[4].parse().unwrap();
    [a, b, c, d]
}

fn parse(data: String) -> (Vec<Sample>, Vec<[usize; 4]>) {
    let before_re = Regex::new(r"Before: \[(\d+), (\d+), (\d+), (\d+)\]").unwrap();
    let after_re= Regex::new(r"After:  \[(\d+), (\d+), (\d+), (\d+)\]").unwrap();
    let op_re = Regex::new(r"(\d+) (\d+) (\d+) (\d+)").unwrap();
    let mut state = SampleParseState::Before;
    let mut current = Sample::new();
    let mut samples = Vec::new();
    let mut program = Vec::new();
    for line in data.lines() {
        match state {
            SampleParseState::Before => {
                if let Some(captures) = before_re.captures(line) {
                    current.set_before(captures_to_arr(&captures));
                    state = SampleParseState::Opcode
                }
                if let Some(captures) = op_re.captures(line) {
                    program.push(captures_to_arr(&captures));
                }
            },
            SampleParseState::Opcode => {
                if let Some(captures) = op_re.captures(line) {
                    current.set_op(captures_to_arr(&captures));
                    state = SampleParseState::After
                }

            },
            SampleParseState::After => {
                if let Some(captures) = after_re.captures(line) {
                    current.set_after(captures_to_arr(&captures));
                    state = SampleParseState::Before;
                    samples.push(current);
                    current = Sample::new();

                }
            }
        }
    }
    (samples, program)
}



macro_rules! check {
    ($s:expr, $k:ident) => {
        {
            let mut computer = Computer::from_arr($s.before());
            let op = $s.op();
            computer.$k(op[1], op[2], op[3]);
            if computer.registers == $s.after() { 1 } else { 0 }
        }
    }
}

macro_rules! eval {
    ($h:expr, $s:expr, $k:ident) => {
        {
            if check!($s, $k) == 1 {
                let op = $s.op();
                if let Some(ops) = $h.get_mut(stringify!($k)) {
                    if !ops.contains_key(&op[0]) {
                        ops.insert(op[0], true);
                    }
                } else {
                    let mut ops = HashMap::new();
                    ops.insert(op[0], true);
                    $h.insert(stringify!($k).to_string(), ops);
                }
            } else {
                let op = $s.op();
                if let Some(ops) = $h.get_mut(stringify!($k)) {
                    ops.insert(op[0], false);
                }
            }
        }
    }
}

fn part1(samples: &Vec<Sample>) {
    let mut sample_count = 0;
    for sample in samples {
        let mut check_count = 0;
        check_count += check!(sample, addr);
        check_count += check!(sample, addi);
        check_count += check!(sample, mulr);
        check_count += check!(sample, muli);
        check_count += check!(sample, banr);
        check_count += check!(sample, bani);
        check_count += check!(sample, borr);
        check_count += check!(sample, bori);
        check_count += check!(sample, setr);
        check_count += check!(sample, seti);
        check_count += check!(sample, gtir);
        check_count += check!(sample, gtri);
        check_count += check!(sample, gtrr);
        check_count += check!(sample, eqir);
        check_count += check!(sample, eqri);
        check_count += check!(sample, eqrr);
        if check_count >= 3 {
            sample_count += 1;
        }
    }
    println!("Part1: {}", sample_count);
}

fn generate_mapping(samples: &Vec<Sample>) -> HashMap<usize, String> {
    let mut sample_codes: HashMap<String, HashMap<usize, bool>> = HashMap::new();
    for sample in samples {
        eval!(sample_codes, sample, addr);
        eval!(sample_codes, sample, addi);
        eval!(sample_codes, sample, mulr);
        eval!(sample_codes, sample, muli);
        eval!(sample_codes, sample, banr);
        eval!(sample_codes, sample, bani);
        eval!(sample_codes, sample, borr);
        eval!(sample_codes, sample, bori);
        eval!(sample_codes, sample, setr);
        eval!(sample_codes, sample, seti);
        eval!(sample_codes, sample, gtir);
        eval!(sample_codes, sample, gtri);
        eval!(sample_codes, sample, gtrr);
        eval!(sample_codes, sample, eqir);
        eval!(sample_codes, sample, eqri);
        eval!(sample_codes, sample, eqrr);
    }

    // remove the false ones
    for (_, codes) in sample_codes.iter_mut() {
        codes.retain(|_op, good| *good);
    }
    // now find versions with only 1 code and use them to remove the others
    let mut opcodes = HashMap::new();
    loop {
        for (function, codes) in sample_codes.iter() {
            if codes.len() == 1 {
                for (code, _) in codes {
                    opcodes.insert(*code, function.to_string());
                }
            }
        }
        for (_function, codes) in sample_codes.iter_mut() {
            for (code, _) in opcodes.iter() {
                codes.remove(code);
            }
        }

        for (_, function) in opcodes.iter() {
            sample_codes.remove(function);
        }

        if sample_codes.is_empty() {
            break;
        }
    }
    opcodes
}

fn part2(samples: &Vec<Sample>, program: &Vec<[usize; 4]>) {
    let mapping = generate_mapping(samples);
    let mut computer = Computer::new();
    for op in program {
        computer.exec(&mapping, op);
    }
    println!("Part2 {}", computer.registers[0]);
}


fn main() {
    let data = read_to_string("input").expect("Failed to open input");
    let (samples, program) = parse(data);
    part1(&samples);
    part2(&samples, &program);
}
