use std::fs::read_to_string;

fn collapse_polymers(data: String) -> String {
    let mut data = data;
    loop {
        let mut polymers = String::new();
        let last = data.chars().fold(None, |last_unit: Option<char>, unit| {
            if let Some(last_unit) = last_unit {
                if last_unit != unit && last_unit.to_ascii_lowercase() == unit.to_ascii_lowercase() {
                    None
                } else {
                    polymers.push(last_unit);
                    Some(unit)
                }
            } else {
                Some(unit)
            }
        });
        if let Some(last) = last {
            polymers.push(last);
        }
        if data.len() == polymers.len() { break; }
        data = polymers;
    }
    data
}

fn part1() {
    let data = read_to_string("input").expect("Failed to open file").trim().to_string();
    let data = collapse_polymers(data);
    println!("{}", data.len());
}

fn part2() {
    let data = read_to_string("input").expect("Failed to open file").trim().to_string();
    let lower_ascii = "abcdefghijklmnopqrstuvwxyz";
    let mut lowest = 0;
    for test_unit in lower_ascii.chars() {
        let polymers = data.chars().filter_map(|unit| {
           if test_unit == unit.to_ascii_lowercase() {
               None
           } else {
               Some(unit)
           }
        }).collect();
        let data = collapse_polymers(polymers);
        if lowest == 0 || data.len() < lowest {
            lowest = data.len()
        }
    }
    println!("{}", lowest);
}

fn main() {
    part1();
    part2();
}
