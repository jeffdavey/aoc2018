extern crate regex;

use std::fs::read_to_string;
use regex::Regex;


fn part1() {
    let data = read_to_string("input").expect("Failed to open file");
    let mut fabric = vec![vec![0u8; 1000]; 1000];
    let re = Regex::new(r"#(\d+) @ (\d+),(\d+): (\d+)x(\d+)").expect("Regex failed");

    data.lines().filter_map(|line| {
        re.captures(line)
    }).for_each(|captures| {
        //println!("{} {} {} {} {}", &captures[0], &captures[1], &captures[2], &captures[3], &captures[4]);
        let id = captures[1].parse::<usize>().unwrap();
        let x = captures[2].parse::<usize>().unwrap();
        let y = captures[3].parse::<usize>().unwrap();
        let w = captures[4].parse::<usize>().unwrap();
        let h = captures[5].parse::<usize>().unwrap();
        for i in 0..w {
            for j in 0..h {
                fabric[x + i][y + j] += 1
            }
        }
    });

    let mut inches = 0;
    for x in 0..1000 {
        for y in 0..1000 {
            if fabric[x][y] > 1 {
                inches += 1;
            }
        }
    }
    println!("Found {}", inches);
}

fn part2() {
    let data = read_to_string("input").expect("Failed to open file");
    let mut fabric = vec![vec![0u16; 1000]; 1000];
    let re = Regex::new(r"#(\d+) @ (\d+),(\d+): (\d+)x(\d+)").expect("Regex failed");

    data.lines().filter_map(|line| {
        re.captures(line)
    }).for_each(|captures| {
        //println!("{} {} {} {} {}", &captures[0], &captures[1], &captures[2], &captures[3], &captures[4]);
        let id = captures[1].parse::<usize>().unwrap();
        let x = captures[2].parse::<usize>().unwrap();
        let y = captures[3].parse::<usize>().unwrap();
        let w = captures[4].parse::<usize>().unwrap();
        let h = captures[5].parse::<usize>().unwrap();
        for i in 0..w {
            for j in 0..h {
                fabric[x + i][y + j] += 1
            }
        }
    });


    data.lines().filter_map(|line| {
        re.captures(line)
    }).for_each(|captures| {
        //println!("{} {} {} {} {}", &captures[0], &captures[1], &captures[2], &captures[3], &captures[4]);
        let id = captures[1].parse::<usize>().unwrap();
        let x = captures[2].parse::<usize>().unwrap();
        let y = captures[3].parse::<usize>().unwrap();
        let w = captures[4].parse::<usize>().unwrap();
        let h = captures[5].parse::<usize>().unwrap();
        let mut found = true;
        for i in 0..w {
            for j in 0..h {
                if fabric[x + i][y + j] != 1 {
                    found = false;
                    break;
                }
            }
            if !found {
                break;
            }
        }
        if found {
            println!("Found {}", id);
            return;
        }
    });

}

fn main() {
    part1();
    part2();
}