extern crate failure;

use std::fs::File;
use std::io::{BufRead, BufReader};
use failure::Error;

fn main() -> Result<(), Error> {
    let mut sum: i64 = 0;
    let file = File::open("input")?;
    for line in BufReader::new(file).lines() {
        let line = line?;
        let val: i64 = line.parse()?;
        sum += val;
    }
    println!("Output: {}", sum);
    Ok(())
}