use std::fs::read_to_string;
use std::cmp::Ordering;
use std::io::{self, Write};
use colored::*;
use std::thread;

enum Track {
    Vertical,
    Horizontal,
    CurveRight,
    CurveLeft,
    Intersection,
    Empty,
}

#[derive(Debug, Clone, Copy)]
enum Direction {
    Left,
    Right,
    Up,
    Down
}

#[derive(Debug, Clone, Copy)]
struct Cart {
    direction: Direction,
    x: usize,
    y: usize,
    sequence: usize,
    id: usize,
    is_alive: bool,
}


fn turn_left(cart: &mut Cart) {
    match cart.direction {
        Direction::Left => {
            cart.direction = Direction::Down;
            cart.y += 1;
        },
        Direction::Right => {
            cart.direction = Direction::Up;
            cart.y -= 1;
        },
        Direction::Up => {
            cart.direction = Direction::Left;
            cart.x -= 1;
        },
        Direction::Down => {
            cart.direction = Direction::Right;
            cart.x += 1;
        }
    }
}

fn turn_right(cart: &mut Cart) {
    match cart.direction {
        Direction::Left => {
            cart.direction = Direction::Up;
            cart.y -= 1;
        },
        Direction::Right => {
            cart.direction = Direction::Down;
            cart.y += 1;
        },
        Direction::Up => {
            cart.direction = Direction::Right;
            cart.x += 1;
        },
        Direction::Down => {
            cart.direction = Direction::Left;
            cart.x -= 1;
        }
    }
}

fn go_straight(cart: &mut Cart) {
    match cart.direction {
        Direction::Left => {
            cart.x -= 1;
        },
        Direction::Right => {
            cart.x += 1;
        },
        Direction::Up => {
            cart.y -= 1;
        },
        Direction::Down => {
            cart.y += 1;
        }
    }
}

fn curve_left(cart: &mut Cart) {
    match cart.direction {
        Direction::Down | Direction::Up => turn_left(cart),
        Direction::Left | Direction::Right => turn_right(cart)
    }
}

fn curve_right(cart: &mut Cart) {
    match cart.direction {
        Direction::Down | Direction::Up => turn_right(cart),
        Direction::Left | Direction::Right => turn_left(cart)
    }
}


fn check_collisions(entities: &Vec<Cart>) -> bool {
    for i in 0..entities.len() {
        for j in 0..entities.len() {
            if i != j && entities[i].x == entities[j].x && entities[i].y == entities[j].y {
                println!("Collision at {},{}", entities[i].x, entities[i].y);
                return true;
            }
        }
    }
    return false;
}

fn remove_collisions(entities: &mut Vec<Cart>) -> bool{
    for i in 0..entities.len() {
        for j in 0..entities.len() {
            if i != j && entities[i].x == entities[j].x && entities[i].y == entities[j].y && entities[i].is_alive && entities[j].is_alive {
                println!("Collision - {:?},{:?}", entities[i], entities[j]);
                entities[i].is_alive = false;
                entities[j].is_alive = false;
                return true;
            }
        }
    }
    return false;
}


fn get_char(track: &Track, x: usize, y: usize, entities: &Vec<Cart>) -> String {
    for cart in entities {
        if cart.x == x && cart.y == y {
            return match cart.direction {
                Direction::Right => ">".bright_cyan().to_string(),
                Direction::Left => "<".bright_cyan().to_string(),
                Direction::Up => "^".bright_cyan().to_string(),
                Direction::Down => "v".bright_cyan().to_string(),
            }
        }
    }
    match track {
        Track::CurveLeft => "\\".to_string(),
        Track::CurveRight => "/".to_string(),
        Track::Horizontal => "-".to_string(),
        Track::Vertical => "|".to_string(),
        Track::Intersection => "+".to_string(),
        Track::Empty => " ".to_string()
    }
}
fn draw(grid: &Vec<Vec<Track>>, entities: &Vec<Cart>) {
    let stdout = io::stdout();
    let mut handle = stdout.lock();
    for y in 0..grid.len() {
        for x in 0..grid[y].len() {
            handle.write(&get_char(&grid[y][x], x, y, entities).into_bytes()).unwrap();
        }
        handle.write(&[10u8]).unwrap();
    }
}

fn part1(grid: &Vec<Vec<Track>>, mut entities: Vec<Cart>) {
    loop {
        //draw(grid, &entities);
        entities.sort_by(|a,b| {
            let mut rtn = a.y.cmp(&b.y);
            if rtn == Ordering::Equal {
                rtn = a.x.cmp(&b.x);
            }
            rtn
        });

        for i in 0..entities.len() {
            let cart = &mut entities[i];
            match grid[cart.y][cart.x] {
                Track::Empty => panic!(format!("Reached empty at {},{}", cart.x, cart.y)),
                Track::Intersection => {
                    match cart.sequence {
                        0 => {
                            turn_left(cart);
                            cart.sequence += 1;
                        },
                        1 => {
                            go_straight(cart);
                            cart.sequence += 1;
                        },
                        2 => {
                            turn_right(cart);
                            cart.sequence = 0;
                        }
                        _ => unreachable!()
                    }
                },
                Track::Vertical => go_straight(cart),
                Track::Horizontal => go_straight(cart),
                Track::CurveLeft => curve_left(cart),
                Track::CurveRight => curve_right(cart),
            }

            if check_collisions(&entities) {
                return;
            }
        }
    }
}

fn part2(grid: &Vec<Vec<Track>>, mut entities: Vec<Cart>) {
    loop {
        //draw(grid, &entities);
        entities.sort_by(|a, b| {
            let mut rtn = a.y.cmp(&b.y);
            if rtn == Ordering::Equal {
                rtn = a.x.cmp(&b.x);
            }
            rtn
        });

        let mut num_alive = 0;
        for i in 0..entities.len() {
            {
                let cart = &mut entities[i];
                match grid[cart.y][cart.x] {
                    Track::Empty => panic!(format!("Reached empty at {},{}", cart.x, cart.y)),
                    Track::Intersection => {
                        match cart.sequence {
                            0 => {
                                turn_left(cart);
                                cart.sequence += 1;
                            },
                            1 => {
                                go_straight(cart);
                                cart.sequence += 1;
                            },
                            2 => {
                                turn_right(cart);
                                cart.sequence = 0;
                            }
                            _ => unreachable!()
                        }
                    },
                    Track::Vertical => go_straight(cart),
                    Track::Horizontal => go_straight(cart),
                    Track::CurveLeft => curve_left(cart),
                    Track::CurveRight => curve_right(cart),
                }
            }

            remove_collisions(&mut entities);
            if entities[i].is_alive {
                num_alive += 1;
            }
        }
        if num_alive == 1 {
            println!("No more carts, last {:?}", entities[0]);
            return;
        }

    }
}

fn main() {
    let mut entities = Vec::new();
    let data = read_to_string("input").expect("Failed to read input");
    let mut index = 0;
    let grid = data.lines().enumerate().map(|(y, l)| {
        l.chars().enumerate().map(|(x, c)| {
            match c {
                '|' => Track::Vertical,
                '-' => Track::Horizontal,
                '/' => Track::CurveRight,
                '\\' => Track::CurveLeft,
                ' ' => Track::Empty,
                '+' => Track::Intersection,
                '>' => {
                    entities.push(Cart {
                        direction: Direction::Right,
                        x,
                        y,
                        sequence: 0,
                        id: index,
                        is_alive: true,
                    });
                    index += 1;
                    Track::Horizontal
                },
                '<' => {
                    entities.push(Cart {
                        direction: Direction::Left,
                        x,
                        y,
                        sequence: 0,
                        id: index,
                        is_alive: true,
                    });
                    index += 1;
                    Track::Horizontal
                },
                '^' => {
                    entities.push(Cart {
                        direction: Direction::Up,
                        x,
                        y,
                        sequence: 0,
                        id: index,
                        is_alive: true,
                    });
                    index += 1;
                    Track::Vertical
                },
                'v' => {
                    entities.push(Cart {
                        direction: Direction::Down,
                        x,
                        y,
                        sequence: 0,
                        id: index,
                        is_alive: true,
                    });
                    index += 1;
                    Track::Vertical
                },
                _ => unreachable!()
            }
        }).collect::<Vec<_>>()
    }).collect::<Vec<_>>();
    println!("Grid size is {}x{}", grid.len(), grid[0].len());

    part1(&grid, entities.to_vec());
    part2(&grid, entities.to_vec());
}
