extern crate failure;

use std::fs::File;
use std::io::{BufRead, BufReader, Read};
use failure::Error;

fn main() -> Result<(), Error> {
    let mut data = Vec::new();
    let mut file = File::open("input")?;
    file.read_to_end(&mut data)?;
    let reader = BufReader::new(data.as_slice());
    for line in reader.lines() {
        let line = line?;
        let reader2 = BufReader::new(data.as_slice());
        for line2 in reader2.lines() {
            let line2 = line2?;
            if line != line2 {
                let mut non_match = 0;
                let arr1= line.as_bytes();
                let arr2 = line2.as_bytes();
                for i in 0..arr1.len() {
                    if arr2[i] != arr1[i] {
                        non_match += 1;
                    }
                    if non_match > 1 {
                        break;
                    }
                }
                if non_match == 1 {
                    let result: String = line.chars().zip(line2.chars()).filter(|&(c1, c2)| c1 == c2).map(|(c, _)| c).collect();
                    println!("Found {} {} = {}", line, line2, result);
                    break;
                }
            }
        }
    }
    Ok(())
}