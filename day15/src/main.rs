use std::fs::read_to_string;
use pathfinding::prelude::*;
use std::cmp::Ordering;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum Race {
    Goblin,
    Elf,
}

#[derive(Debug, Copy, Clone)]
struct Entity {
    x: usize,
    y: usize,
    health: i64,
    damage: i64,
    id: usize,
    race: Race,
}

impl Entity {
    pub fn new(id: usize, x: usize, y: usize, race: Race) -> Self {
        Self {
            id,
            x,
            y,
            race,
            damage: 3,
            health: 200,
        }
    }
}
#[derive(Debug, Copy, Clone)]
enum Tile {
    Wall,
    Open,
}

fn read_map(path: &str) -> (Vec<Vec<Tile>>, Vec<Entity>) {
    let data = read_to_string(path).expect("Failed to read input");
    let lines = data.lines();
    let mut entities = Vec::new();
    let mut y_vec= Vec::new();
    let mut entity_id = 0;
    for (y, line) in lines.enumerate() {
        let mut x_vec = Vec::new();
        for (x, tile) in line.chars().enumerate() {
            match tile {
                '#' => x_vec.push(Tile::Wall),
                '.' => x_vec.push(Tile::Open),
                'G' => {
                    entities.push(Entity::new(entity_id, x, y, Race::Goblin));
                    entity_id += 1;
                    x_vec.push(Tile::Open);
                },
                'E' => {
                    entities.push(Entity::new(entity_id, x, y, Race::Elf));
                    x_vec.push(Tile::Open);;
                    entity_id += 1;
                },
                _ => unreachable!()
            }
        }
        y_vec.push(x_vec);
    }
    (y_vec, entities)
}


fn find_targets<'a>(entity: &'a Entity, entities: &'a Vec<Entity>, adjacent: bool) -> Vec<&'a Entity> {
    let mut targets = Vec::new();
    for target in entities {
        if target.race != entity.race && target.health > 0 {
            if !adjacent || is_adjacent(entity.x, entity.y, target.x, target.y) {
                targets.push(target);
            }
        }
    }
    targets
}

fn entity_at_pos(entities: &Vec<Entity>, x: usize, y: usize) -> bool {
    for entity in entities {
        if entity.health > 0 && entity.x == x && entity.y == y {
            return true;
        }
    }
    false
}

fn get_positions(map: &Vec<Vec<Tile>>, entities: &Vec<Entity>, x: usize, y: usize) -> Vec<(usize, usize)> {
    let mut rtn = Vec::new();
    if y - 1 > 0 {
        if let Tile::Open = map[y-1][x] {
            if !entity_at_pos(entities, x, y-1) {
                rtn.push((x, y - 1));
            }
        }
    }
    if x - 1 > 0 {
        if let Tile::Open = map[y][x-1] {
            if !entity_at_pos(entities, x - 1, y) {
                rtn.push((x - 1, y));
            }
        }
    }
    if x + 1 < map[y].len() {
        if let Tile::Open = map[y][x+1] {
            if !entity_at_pos(entities, x + 1, y) {
                rtn.push((x + 1, y));
            }
        }
    }
    if y + 1 < map.len() {
        if let Tile::Open = map[y+1][x] {
            if !entity_at_pos(entities, x, y + 1) {
                rtn.push((x, y + 1));
            }
        }
    }
    rtn
}

fn is_adjacent(x: usize, y: usize, target_x: usize, target_y: usize) -> bool {
    let x_diff = i64::abs(x as i64 - target_x as i64);
    let y_diff = i64::abs(y as i64 - target_y as i64);
    if x_diff + y_diff == 1 {
        true
    } else {
        false
    }
}

fn get_path(map: &Vec<Vec<Tile>>, entity: &Entity, entities: &Vec<Entity>) -> (Option<Vec<(usize, usize)>>, bool) {
    let targets = find_targets(entity, entities, false);
    let finished = targets.is_empty();
    let mut paths = Vec::new();
    for target in &targets {
        if is_adjacent(entity.x, entity.y, target.x, target.y) {
            return (None, finished);
        }
        let positions = get_positions(map, entities, target.x, target.y);
        for pos in positions {
            if let Some(path) = bfs(&(entity.x, entity.y), |&(x, y)| get_positions(map, entities, x, y).into_iter(), |&p| p == pos) {
                paths.push(path)
            }
        }
    }
    if !paths.is_empty() {
        // sort
        paths.sort_by(|a_path, b_path| {
            let mut res = a_path.len().cmp(&b_path.len());
            if res == Ordering::Equal {
                let end_a = a_path[a_path.len() - 1];
                let end_b = b_path[b_path.len() - 1];
                res = end_a.1.cmp(&end_b.1);
                if res == Ordering::Equal {
                    res = end_a.0.cmp(&end_b.0);
                }
            }
            res
        });
        (Some(paths[0].to_vec()), finished)
    } else {
        (None, finished)
    }
}

fn find_attack_target(entity: &Entity, entities: &Vec<Entity>) -> Option<usize> {
    let mut targets = find_targets(entity, entities, true);
    if !targets.is_empty() {
        targets.sort_by(|a,b| {
            let mut res = a.health.cmp(&b.health);
            if res == Ordering::Equal {
                res = a.y.cmp(&b.y);
                if res == Ordering::Equal {
                    res = a.x.cmp(&b.x);
                }
            }
            res
        });
        Some(targets[0].id)
    } else {
        None
    }
}

fn print_map(map: &Vec<Vec<Tile>>, entities: &Vec<Entity>, round: usize) {
    let sum: i64 = entities.iter().filter_map(|e| if e.health > 0 { Some(e.health) } else { None }).sum();
    println!("Round {} - {}", round, sum);
    for y in 0..map.len() {
        for x in 0..map[y].len() {
            let mut entity_found = false;
            for entity in entities {
                if entity.health > 0 && entity.x == x && entity.y == y {
                    match entity.race {
                        Race::Goblin => print!("G"),
                        Race::Elf => print!("E")
                    }
                    entity_found = true;
                    break;
                }
            }
            if !entity_found {
                match map[y][x] {
                    Tile::Open => print!("."),
                    Tile::Wall => print!("#"),
                }
            }
        }
        println!();
    }
    for entity in entities {
        let race = match entity.race {
            Race::Goblin => "G",
            Race::Elf => "E"
        };
        println!("{}({}) {},{} - {}", race, entity.id, entity.x, entity.y, entity.health);
    }
    println!();
}


fn run(map: &Vec<Vec<Tile>>, entities: &mut Vec<Entity>) -> bool {
    // turn order
    entities.sort_by(|a,b| {
        let mut res = a.y.cmp(&b.y);
        if res == Ordering::Equal {
            res = a.x.cmp(&b.x);
        }
        res
    });
    for i in 0..entities.len() {
        if entities[i].health > 0 {
            // move
            let (path, finished) = get_path(&map, &entities[i], &entities);
            if finished {
                return false;
            }
            if let Some(path) = path {
                entities[i].x = path[1].0;
                entities[i].y = path[1].1;
            }
            // attack
            if let Some(target_id) = find_attack_target(&entities[i], &entities) {
                for j in 0..entities.len() {
                    if entities[j].id == target_id {
                        entities[j].health -= entities[i].damage;
                    }
                }
            }
        }
    }
    true
}

fn part1(map: Vec<Vec<Tile>>, mut entities: Vec<Entity>) {
    let mut round = 0;
    print_map(&map, &entities, round);
    while run(&map, &mut entities) {
        round += 1;
        print_map(&map, &entities, round);
    }
    let sum: i64 = entities.iter().filter_map(|e| if e.health > 0 { Some(e.health) } else { None }).sum();
    println!("Part1 {} * {} = {}", sum, round, sum * round as i64);

}

fn part2(map: Vec<Vec<Tile>>, entities: Vec<Entity>) {
    let mut elf_power = 4;
    loop {
        let mut entities = entities.to_vec();
        for entity in &mut entities {
            if entity.race == Race::Elf {
                entity.damage = elf_power;
            }
        }
        let mut round = 0;
        print_map(&map, &entities, round);
        while run(&map, &mut entities) {
            round += 1;
            print_map(&map, &entities, round);
        }
        let mut elves_dead = 0;
        for entity in &entities {
            if entity.race == Race::Elf && entity.health <= 0 {
                elves_dead += 1;
            }
        }
        let sum: i64 = entities.iter().filter_map(|e| if e.health > 0 { Some(e.health) } else { None }).sum();
        println!("Part2 power: {} dead: {}, sum: {} * round: {} = value: {}", elf_power, elves_dead, sum, round, sum * round as i64);
        elf_power += 1;
        if elves_dead == 0 {
            break;
        }
    }
}

fn main() {
    let (map, entities) = read_map("input");
    part1(map.to_vec(), entities.to_vec());
    part2(map.to_vec(), entities.to_vec());
}
